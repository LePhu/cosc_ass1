#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void){
    int i, waiting;
    int fd1[2]; // Used to store two ends of first pipe 
    int fd2[2]; // Used to store two ends of second pipe 
    int fd3[2]; // Used to store two ends of second pipe 
    int fd4[2];
    int fd5[2],fd6[2],fd7[2];    // Used to store two ends of second pipe 
    pipe(fd1);
    pipe(fd2);
    pipe(fd3);
    pipe(fd4);
    pipe(fd5);
    pipe(fd6);
    pipe(fd7);
    
    int a,b,c,d,p0=0,p1=0,p2=0,p3=0;
    pid_t pid[4];
    

    for (i=0;i<4;i++) {
        pid[i] = fork();
        if (pid[i] == 0) {
            break;
        }
    }

    if (pid[0] != 0 && pid[1] != 0 && pid[2] != 0 && pid[3] != 0) {
        // That's the father, it waits for all the childs
        printf("I'm the father [pid: %d, ppid: %d]\n",getpid(),getppid());
        
        //test data
         a=2;
         b=3;
         c=1;
         d=8;
         close(fd5[0]);
         close(fd6[0]);
         close(fd7[0]);
        close(fd1[0]); //Close reading end of first pipe
        write(fd1[1], &a, sizeof(a)); //Wrrite data end of first pipe
        write(fd1[1], &b, sizeof(b)); //Wrrite data end of first pipe
        write(fd1[1], &c, sizeof(c)); //Wrrite data end of first pipe
        write(fd1[1], &d, sizeof(d)); //Wrrite data end of first pipe
        //------------------------------------------------------
        close(fd2[0]); //Close reading end of first pipe
        write(fd2[1], &a, sizeof(a)); //Wrrite data end of first pipe
        write(fd2[1], &b, sizeof(b)); //Wrrite data end of first pipe
        write(fd2[1], &c, sizeof(c)); //Wrrite data end of first pipe
        write(fd2[1], &d, sizeof(d)); //Wrrite data end of first pipe
        //----------------------------------------------------------------------
        close(fd3[0]); //Close reading end of first pipe
        write(fd3[1], &a, sizeof(a)); //Wrrite data end of first pipe
        write(fd3[1], &b, sizeof(b)); //Wrrite data end of first pipe
        write(fd3[1], &c, sizeof(c)); //Wrrite data end of first pipe
        write(fd3[1], &d, sizeof(d)); //Wrrite data end of first pipe
        //-------------------------------------------------------------------
        close(fd4[0]); //Close reading end of first pipe
        write(fd4[1], &a, sizeof(a)); //Wrrite data end of first pipe
        write(fd4[1], &b, sizeof(b)); //Wrrite data end of first pipe
        write(fd4[1], &c, sizeof(c)); //Wrrite data end of first pipe
        write(fd4[1], &d, sizeof(d)); //Wrrite data end of first pipe
                  
        //waiting fo child process
        for(i=0;i<4;i++) {
            wait(&waiting);
        }
        
        close(fd1[1]);  
         close(fd2[1]);  
          close(fd3[1]);  
           close(fd4[1]);  
        printf("%d\n",a);
        printf("%d\n",b);
        printf("%d\n",c);
        printf("%d\n",d);
        
    } 
    // 4 child process 
   /*if (pid[0] == 0 && pid[1] != 0 && pid[2] != 0 && pid[3] != 0) {
        
        printf("I'm P0 of the children [pid: %d, ppid: %d]\n",getpid(),getppid());
    }*/
    else {
    //PROCESS 1 (P0)-----------------------------------------------------------
        if(pid[0]==0 ){
         printf("I'm p0 of the children [pid: %d, ppid: %d]\n",getpid(),getppid());
        //Write end read data from father process
        close(fd1[1]); //Close writing end of first pipe
        read(fd1[0],&a,sizeof(a)); // open read end of first pipe
        //close(fd2[1]); //Close writing end of first pipe
        read(fd1[0],&b,sizeof(b)); // open read end of first pipe
        read(fd1[0],&c,sizeof(c)); // open read end of first pipe
        read(fd1[0],&d,sizeof(d)); // open read end of first pipe
        p0=a-b;
         //test value all
         
         close(fd1[0]);
        // close(fd2[0]);
         //test
         //printf("%d\n",a);
          //printf("%d\n",b);
         printf("P0=a-b=");
         printf("%d\n",p0);
         close(fd5[0]);
         close(fd6[0]);
         close(fd7[0]);
    //write value p0 in end of pipe
         write(fd5[1],&p0,sizeof(p0));
         close(fd5[1]);
         //text fd5 in process
         //printf("fd5");
        // printf("%d\n",p0);
         
        
       
        }
    //PROCESS 2 (P1)---------------------------------------------------------------
        if(pid[1]==0 ){
         printf("I'm p1 of the children [pid: %d, ppid: %d]\n",getpid(),getppid());
        //Write end read data from father process
        close(fd2[1]); //Close writing end of first pipe
        read(fd2[0],&a,sizeof(a)); // open read end of first pipe
        //close(fd2[1]); //Close writing end of first pipe
        read(fd2[0],&b,sizeof(b)); // open read end of first pipe
        read(fd2[0],&c,sizeof(c)); // open read end of first pipe
        read(fd2[0],&d,sizeof(d)); // open read end of first pipe
        p1=c+d;
         //test value all
         
         close(fd2[0]);
        // close(fd2[0]);
         //test
        // printf("%d\n",c);
         // printf("%d\n",d);
         printf("P1=c+d=");
         printf("%d\n",p1);
    
    //write value p0 in end of pipe
         write(fd6[1],&p1,sizeof(p1));
         close(fd6[1]);
         //text fd6 in process
        // printf("fd6");
         //printf("%d\n",p1);
         
        }
    //PROCESS 3 (P2)------------------------------------------------------------
        if(pid[2]==0 & pid[3]==0 & pid[0] !=0 & pid[1] !=0 ) {
            printf("I'm p2 of the children [pid: %d, ppid: %d]\n",getpid(),getppid());
            close(fd3[1]); //close writrng end of pipe
            read(fd3[0],&a,sizeof(a));// open read end of pipe
            read(fd3[0],&b,sizeof(a));
            read(fd3[0],&c,sizeof(a));
            read(fd3[0],&d,sizeof(a));
            p2=a-d;
            close(fd3[0]);
       // printf("%d\n",a);
       // printf("%d\n",d);
        printf("P2=a-d=");
        printf("%d\n",p2);
    //write value p0 in end of pipe
       
         write(fd7[1],&p2,sizeof(p2));
         close(fd7[1]);
         //text fd5 in process
         //printf("fd7");
       //  printf("%d\n",p2);
        
        }
    
    //PROCESS 4 (P3)----------------------------------------------------------------------
         
        if(pid[2]!=0 & pid[3]==0 & pid[0] !=0 & pid[1] !=0 ) 
        {
        printf("I'm p3 of the children [pid: %d, ppid: %d]\n",getpid(),getppid());
            
            close(fd5[1]); //close writrng end of pipe
            close(fd6[1]);
            close(fd7[1]);
            read(fd5[0],&p0,sizeof(p0));
            read(fd6[0],&p1,sizeof(p1));
            read(fd7[0],&p2,sizeof(p2));
            close(fd5[0]);
            close(fd6[0]);
            close(fd7[0]);
            p3=p0*p1+p2;
            //printf("%d\n",p0);
           // printf("%d\n",p1);
           // printf("%d\n",p2);
            printf("P3 = p0*p1+p2= ");
            printf("%d\n",p3);
        
        
        }
        
    }
    
    return 0;
}